//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/29/23.
//

import Foundation
import Fluent
import Vapor

final class ProductsController {
    func create(_ req: Request) throws -> EventLoopFuture <Product> {
        let product = try req.content.decode(Product.self) // content is the body of the HTTP Request
        return product.create(on: req.db).map { product } // after you create the movie, return as this particular object
    }
    
    func all(_ req: Request) throws -> EventLoopFuture <[Product]> {
        Product.query(on: req.db).all()
    }
    
    func update(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        let product = try req.content.decode(Product.self)

        // flat can return an eventloopfuture
        return Product.find(product.id, on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap {
                $0.name = product.name
                $0.type = product.type
                $0.statue = product.statue
                $0.description = product.description
                $0.price = product.price
                $0.image = product.image
//                    $0.category = product.category
//                    $0.from = product.from
//                    $0.to = product.to
//                    $0.weight = product.weight
                $0.volume = product.volume
                $0.permission = product.permission
                return $0.update(on: req.db).transform(to: .ok)
            }
    }
    
    func delete(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
//        var product = try req.content.decode(Product.self)
        
        return Product.find(req.parameters.get("productid"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap {
                $0.delete(on: req.db)
            }.transform(to: HTTPStatus.ok)
        
    }
    
}
