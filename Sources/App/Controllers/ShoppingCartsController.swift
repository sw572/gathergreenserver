//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/29/23.
//

import Foundation
import Fluent
import Vapor

final class ShoppingCartsController {
    func create(_ req: Request) throws -> EventLoopFuture <ShoppingCart> {
        let shoppingCart = try req.content.decode(ShoppingCart.self) // content is the body of the HTTP Request
        return shoppingCart.create(on: req.db).map { shoppingCart }
    }
    
    // for shopping cart, you can only get one
    func one(_ req: Request) throws -> EventLoopFuture <[ShoppingCart]> {
        let user = try req.content.decode(User.self)
        let userId = user.id! // forced unwrap, may not be a good practice
        
        return ShoppingCart.query(on: req.db)
            .join(User.self, on: \ShoppingCart.$user.$id == \User.$id, method:.inner)
            .join(Product.self, on: \ShoppingCart.$product.$id == \Product.$id)
            .filter(ShoppingCart.self, \ShoppingCart.$user.$id == userId)
            .with(\.$product)
            .all()
    }
    
    // only quantity can be changed
    func update(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        let shoppingCart = try req.content.decode(ShoppingCart.self)
        
        // flat can return an eventloopfuture
        return ShoppingCart.find(shoppingCart.id, on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap {
                $0.quantity = shoppingCart.quantity
                return $0.update(on: req.db).transform(to: .ok)
            }
    }
    
    func delete(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        //        var product = try req.content.decode(Product.self)
        
        return ShoppingCart.find(req.parameters.get("shoppingcartid"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap {
                $0.delete(on: req.db)
            }.transform(to: HTTPStatus.ok)
        
    }
}
