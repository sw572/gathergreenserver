//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/29/23.
//

import Foundation
import Fluent
import Vapor

final class OrdersController {
    func create(_ req: Request) throws -> EventLoopFuture <Order> {
        let order = try req.content.decode(Order.self) // content is the body of the HTTP Request
        return order.create(on: req.db).map { order }
    }
    
    // for shopping cart, you can only get one
    func one(_ req: Request) throws -> EventLoopFuture <[Order]> {
        let user = try req.content.decode(User.self)
        let userId = user.id! // forced unwrap, may not be a good practice
        
        return Order.query(on: req.db)
            .join(User.self, on: \Order.$user.$id == \User.$id, method:.inner)
            .join(Product.self, on: \Order.$product.$id == \Product.$id)
            .filter(Order.self, \Order.$user.$id == userId)
            .with(\.$product)
            .all()
    }
    
    func all(_ req: Request) throws -> EventLoopFuture <[Order]> {
        Order.query(on: req.db).with(\.$product).with(\.$user).all()
    }
}

