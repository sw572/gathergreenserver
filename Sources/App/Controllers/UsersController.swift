//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/29/23.
//

import Foundation
import Fluent
import Vapor

final class UsersController {
    func create(_ req: Request) throws -> EventLoopFuture <User> {
        let user = try req.content.decode(User.self) // content is the body of the HTTP Request
        return user.create(on: req.db).map { user } // after you create the movie, return as this particular object
    }
    
    func all(_ req: Request) throws -> EventLoopFuture <[User]> {
        User.query(on: req.db).all()
    }
    
    func update(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        let user = try req.content.decode(User.self)
        
        // flat can return an eventloopfuture
        return User.find(user.id, on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap {
//                $0.id = user.id
                $0.role = user.role
                $0.firstname = user.firstname
                $0.lastname = user.lastname
                $0.username = user.username
                $0.email = user.email
                $0.password = user.password
                $0.address = user.address
                $0.payment = user.payment
                return $0.update(on: req.db).transform(to: .ok)
            }
    }
    
    func delete(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        //        var product = try req.content.decode(Product.self)
        
        return User.find(req.parameters.get("userid"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap {
                $0.delete(on: req.db)
            }.transform(to: HTTPStatus.ok)
        
    }
}
