//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/29/23.
//

import Foundation
import Fluent
import Vapor

final class InventoriesController {
    func create(_ req: Request) throws -> EventLoopFuture <Inventory> {
        let inventory = try req.content.decode(Inventory.self) // content is the body of the HTTP Request
        return inventory.create(on: req.db).map { inventory } // after you create the movie, return as this particular object
    }
    
    func all(_ req: Request) throws -> EventLoopFuture <[Inventory]> {
        Inventory.query(on: req.db).with(\.$product).all()
    }
    
    // only quantity can be changed
    func update(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        let inventory = try req.content.decode(Inventory.self)
        
        // flat can return an eventloopfuture
        return Inventory.find(inventory.id, on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap {
                $0.quantity = inventory.quantity
                return $0.update(on: req.db).transform(to: .ok)
            }
    }
    
    func delete(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        //        var product = try req.content.decode(Product.self)
        
        return Inventory.find(req.parameters.get("inventoryid"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap {
                $0.delete(on: req.db)
            }.transform(to: HTTPStatus.ok)
        
    }
}
