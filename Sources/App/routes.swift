import Fluent
import Vapor
import Leaf

func routes(_ app: Application) throws {
    let productsController = ProductsController()
    
    app.post("products","post", use: productsController.create)
    app.get("products","get", use: productsController.all)
    app.put("products","put", use: productsController.update)
    app.delete("products",":productid", use: productsController.delete)
    
    let usersController = UsersController()
    
    app.post("users","post", use: usersController.create)
    app.get("users","get", use: usersController.all)
    app.put("users","put", use: usersController.update)
    app.delete("users",":userid", use: usersController.delete)
    
    let inventoriesController = InventoriesController()
    
    app.post("inventories","post", use: inventoriesController.create)
    app.get("inventories","get", use: inventoriesController.all)
    app.put("inventories","put", use: inventoriesController.update) // only the quantity can be changed
    app.delete("inventories",":inventoryid", use: inventoriesController.delete)
    
    let shoppingCartsController = ShoppingCartsController()
    
    app.post("shoppingcarts","post", use: shoppingCartsController.create)
    //HTTPRequest method changed to put -sw572
    app.put("shoppingcarts","get", use: shoppingCartsController.one) // remember to pass in a user
    app.put("shoppingcarts","put", use: shoppingCartsController.update) // only the quantity can be changed
    app.delete("shoppingcarts",":shoppingcartid", use: shoppingCartsController.delete)
    
    let ordersController = OrdersController()
    
    app.post("orders","post", use: ordersController.create)
    app.get("orders","getall", use: ordersController.all)
    //HTTPRequest method changed to put -sw572
    app.put("orders","getone", use: ordersController.one)// remember to pass in a user
    
    // the following routes are for view
    app.get{ req -> EventLoopFuture<View> in
        return req.view.render("HomePage.html")
    }
    
//    app.get("product") { req -> EventLoopFuture<View> in
//        Product.query(on: req.db).all()
//        return req.view.render("index", context)
//    }
    
}
