//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/29/23.
//

import Foundation
import Fluent
import FluentPostgresDriver

struct CreateUser: Migration {
    func prepare(on database: FluentKit.Database) -> NIOCore.EventLoopFuture<Void> {
        database.schema("users") // table name
            .id()
            .field("role", .string, .required)
            .field("firstname", .string)
            .field("lastname", .string)
            .field("username", .string, .required)
            .field("email", .string)
            .field("password", .string, .required)
            .field("address",.string)
            .field("payment", .string)
            .create()
    }
    
    func revert(on database: FluentKit.Database) -> NIOCore.EventLoopFuture<Void> {
        database.schema("users").delete()
    }
    
    
}
