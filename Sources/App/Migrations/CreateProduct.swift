//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/28/23.
//

import Foundation
import Fluent
import FluentPostgresDriver

struct CreateProduct: Migration {
    func prepare(on database: FluentKit.Database) -> NIOCore.EventLoopFuture<Void> {
        database.schema("products") // table name
            .id()
            .field("name", .string, .required)
            .field("type", .string, .required)
            .field("price", .float)
            .field("image", .string)
            .field("permission", .string)
            .field("statue", .string, .required) // wrong spelling
            .field("description", .string)
            .field("volume", .float)
//            .field("category", .string, .required)
//            .field("from", .string)
//            .field("to", .string)
//            .field("weight", .float)
//
//            .field("location", .string)
            .create()
    }
    
    func revert(on database: FluentKit.Database) -> NIOCore.EventLoopFuture<Void> {
        database.schema("products").delete()
    }
    
    
    
}
