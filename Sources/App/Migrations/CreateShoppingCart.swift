//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/29/23.
//

import Foundation
import Fluent
import FluentPostgresDriver

struct CreateShoppingCart: Migration {
    func prepare(on database: FluentKit.Database) -> NIOCore.EventLoopFuture<Void> {
        database.schema("shoppingcarts")
            .id()
            .field("user_id",.uuid,.required,.references("users", "id"))
            .field("product_id",.uuid,.required,.references("products", "id"))
            .field("quantity", .float, .required)
            .create()
    }
    
    func revert(on database: FluentKit.Database) -> NIOCore.EventLoopFuture<Void> {
        database.schema("shoppingcarts").delete()
    }
    
    
}
