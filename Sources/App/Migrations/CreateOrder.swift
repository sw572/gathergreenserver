//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/29/23.
//

import Foundation
import Fluent
import FluentPostgresDriver

struct CreateOrder: Migration {
    func prepare(on database: FluentKit.Database) -> NIOCore.EventLoopFuture<Void> {
        database.schema("orders")
            .id()
            .field("user_id",.uuid,.required,.references("users", "id"))
            .field("product_id",.uuid,.required,.references("products", "id"))
            .field("quantity", .float, .required)
            .field("purchasedtime", .string) // may need to change to timestamp later
            .field("paymentinfo", .string)
        //.field("location", .string)
        //.field("shipinfo", .string)
            .create()
    }
    
    func revert(on database: FluentKit.Database) -> NIOCore.EventLoopFuture<Void> {
        database.schema("orders").delete()
    }
    
    
}
