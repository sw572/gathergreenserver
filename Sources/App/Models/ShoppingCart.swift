//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/29/23.
//

import Foundation
import Fluent
import Vapor

final class ShoppingCart: Model, Content {
    static let schema = "shoppingcarts"
    
    @ID(key: .id)
    var id: UUID?
    
    @Parent(key:"user_id")
    var user: User
    
    @Parent(key:"product_id")
    var product: Product
    
    @Field(key: "quantity")
    var quantity: Float
    
    init() {}
    
    init(userid: UUID, productid: UUID, quantity: Float) {
        self.$user.id = userid
        self.$product.id = productid
        self.quantity = quantity
    }
    
}
