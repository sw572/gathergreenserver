//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/29/23.
//

import Foundation
import Fluent
import Vapor

final class Order: Model, Content {
    static let schema = "orders"
    
    @ID(key: .id)
    var id: UUID?
    
    @Parent(key:"user_id")
    var user: User
    
    @Parent(key:"product_id")
    var product: Product
    
    @Field(key: "quantity")
    var quantity: Float
    
    @Field(key: "purchasedtime")
    var purchasedtime: String
    
    @Field(key: "paymentinfo")
    var paymentinfo: String
    
    // @Field(key: "location")
    // var location: String
    
    // @Field(key: "shipinfo")
    // var shipinfo: String
    
    init() {}
    
    init(userid: UUID, productid: UUID, quantity: Float, purchasedtime: String, paymentinfo: String) {
        self.$user.id = userid
        self.$product.id = productid
        self.quantity = quantity
        self.purchasedtime = purchasedtime
        self.paymentinfo = paymentinfo
    }
    
}
