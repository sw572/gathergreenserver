//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/28/23.
//

import Foundation
import Fluent
import Vapor

final class Product: Model, Content {
    static let schema = "products"
    
    @ID(key: .id)
    var id: UUID?
    
    @Field(key: "name")
    var name: String
    
    @Field(key: "type")
    var type: String
    
    @Field(key: "price")
    var price: Float
    
    @Field(key:"image")
    var image: String
    
    @Field(key: "permission")
    var permission: String
    
    @Field(key: "statue")
    var statue: String
    
    @Field(key: "description")
    var description: String
    
//
//    @Field(key: "category")
//    var category: String
//
//    @Field(key: "from")
//    var from: String
//
//    @Field(key: "to")
//    var to: String
//
//    @Field(key: "weight")
//    var weight: Float
    
    @Field(key: "volume")
    var volume: Float
    
//    @Field(key: "location")
//    var location: String
    
    @Children(for: \.$product)
    var inventories: [Inventory]
    
    @Siblings(through: ShoppingCart.self, from: \.$product, to: \.$user)
    var user_s: [User]
    
    @Siblings(through: Order.self, from: \.$product, to: \.$user)
    var user_o: [User]
    
    init() {}
    init(id: UUID? = nil, name: String, type: String, status: Int, description: String, price: Float, image: String, volume: Float, permission: String) {
        self.id = id
        self.name = name
        self.type = type
        self.statue = statue
        self.permission = permission
        self.description = description
        self.price = price
        self.image = image
//        self.category = category
//        self.from = from
//        self.to = to
//        self.weight = weight
        self.volume = volume
//        self.location = location
    }
}
