//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/29/23.
//

import Foundation
import Fluent
import Vapor

final class User: Model, Content {
    static let schema = "users"
    
    @ID(key: .id)
    var id: UUID?
    
    @Field(key: "role")
    var role: String
    
    @Field(key: "firstname")
    var firstname: String
    
    @Field(key: "lastname")
    var lastname: String
    
    @Field(key: "username")
    var username: String
    
    @Field(key: "email")
    var email: String
    
    @Field(key: "password")
    var password: String
    
    @Field(key: "address")
    var address: String
    
    @Field(key: "payment")
    var payment: String
    
    @Siblings(through: ShoppingCart.self, from: \.$user, to: \.$product)
    var product_s: [Product]
    
    @Siblings(through: Order.self, from: \.$user, to: \.$product)
    var product_o: [Product]
    
    init() {}
    
    init(id: UUID? = nil, role: String, firstname: String, lastname: String, username: String, email: String, password: String, address: String, payment: String) {
        self.id = id
        self.role = role
        self.firstname = firstname
        self.lastname = lastname
        self.username = username
        self.email = email
        self.password = password
        self.address = address
        self.payment = payment
    }
}
