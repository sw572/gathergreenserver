//
//  File.swift
//  
//
//  Created by EGR-ECELOANER4-MBP on 3/29/23.
//

import Foundation
import Fluent
import Vapor

final class Inventory: Model, Content {
    static let schema = "inventories"
    
    @ID(key: .id)
    var id: UUID?
    
    @Parent(key:"product_id")
    var product: Product
    
    @Field(key: "quantity")
    var quantity: Float
    
    init() {}
    
    init(productid: UUID, quantity: Float) {
        self.$product.id = productid
        self.quantity = quantity
    }
    
}
